import React from "react";
import { useState } from "react";
/**
 * If count is 0 display "POOP_TEXT" done
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable done
 * The decrement button should decrease the "count" variable done
 * The "count" variable should never go below 0 done
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  // const count = state.count;
  // let text = POOP_TEXT;

  const [count, setCount] = useState(0);
  const [text, setText] = useState(POOP_TEXT);

  function decrement() {
    if (count > 0) {
      setCount((oldCount) => oldCount - 1);
    }
  }

  function displayPoop() {
    if (count === 0) {
      return text;
    }
  }

  function displayCount() {
    if (count > 0) {
      return count;
    }
  }

  function increment() {
    setCount((oldCount) => oldCount + 1);
  }
  return (
    <div style={styles.app}>
      <h1>{displayCount()}</h1>
      <h3>{displayPoop()}</h3>
      <div style={styles.buttons}>
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
